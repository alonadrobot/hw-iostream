package com.ioStream;

import java.io.*;

public class Service {

        private byte[] byte1;

        public Service(byte[] byte1) {
            this.byte1 = byte1;
        }

        public void write(ByteArrayOutputStream to, FileInputStream from) throws IOException {
                if (to == null || from == null) {
                    throw new IllegalArgumentException();
                }
                int length;
                while ((length = from.read(byte1)) > 0) {
                    to.write(byte1, 0, length);
                }
        }

        public ByteArrayOutputStream getByteArrayOutputStream() {
            return new ByteArrayOutputStream();
        }

        public FileInputStream getFileInputStream(File file) throws IOException {
            return new FileInputStream(file);
        }

        public FileOutputStream getFileOutputStream(File file) throws IOException {
            return new FileOutputStream(file);
        }
    }

