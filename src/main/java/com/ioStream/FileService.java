package com.ioStream;


import java.io.*;

public class FileService {

    private Service Service;

    public FileService(Service Service) {
        this.Service = Service;
    }

    public void writeFile(File file, File[] ar) throws IOException {
        if (ar == null || ar.length == 0 || file == null ||  ar.length > 127) {
            throw new IllegalArgumentException();
        }
        ByteArrayOutputStream byteArrayOutputStream = Service.getByteArrayOutputStream();
            for (int i = 0; i < ar.length; i++) {
                if (ar[i] == null) {
                    continue;
                }
                FileInputStream fileInputStream = Service.getFileInputStream(ar[i]);
                    Service.write(byteArrayOutputStream, fileInputStream);
            }
          FileInputStream fileInputStream = Service.getFileInputStream(file);
                Service.write(byteArrayOutputStream, fileInputStream);
            FileOutputStream fileOutputStream = Service.getFileOutputStream(file);
                byteArrayOutputStream.writeTo(fileOutputStream);
    }

    public void toStart(File file, File[] ar) throws IOException {
        if (file == null || ar == null || ar.length == 0 || ar.length > 127) {
            throw new IllegalArgumentException();
        }
        ByteArrayOutputStream byteArrayOutputStream = Service.getByteArrayOutputStream();
        for (int i = 0; i < ar.length; i++) {
            if (ar[i] == null) {
                continue;
            }
            FileInputStream fileInputStream = Service.getFileInputStream(ar[i]);
            Service.write(byteArrayOutputStream, fileInputStream);
        }
        FileInputStream fileInputStream = Service.getFileInputStream(file);
        Service.write(byteArrayOutputStream, fileInputStream);

        FileOutputStream fileOutputStream = Service.getFileOutputStream(file);
        byteArrayOutputStream.writeTo(fileOutputStream);
    }


    public void toEnd(File file, File[] ar) throws IOException {
        if (file == null || ar == null || ar.length == 0 || ar.length > 127) {
            throw new IllegalArgumentException();
        }
      ByteArrayOutputStream byteArrayOutputStream = Service.getByteArrayOutputStream();

            FileInputStream fileInputStream = Service.getFileInputStream(file);
                Service.write(byteArrayOutputStream, fileInputStream);

            for (int i = 0; i < ar.length; i++) {
                if (ar[i] == null) {
                    continue;
                }
                FileInputStream fileInputStream1 = Service.getFileInputStream(ar[i]);
                    Service.write(byteArrayOutputStream, fileInputStream1);

            }

            FileOutputStream outputStream = Service.getFileOutputStream(file);
                byteArrayOutputStream.writeTo(outputStream);

    }
}




