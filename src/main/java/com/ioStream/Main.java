package com.ioStream;

import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        byte[] byte1 = new byte[100];
        byte[] byte2 = new byte[50];
        Service Service = new Service(byte1);
        FileService FileService = new FileService(Service);

        File f1 = new File("src/main/java/com/ioStream/file/file.txt");
        File f2 = new File("src/main/java/com/ioStream/file/file2.txt");
        File f3 = new File("src/main/java/com/ioStream/file/file3.txt");
        File f4 = new File("src/main/java/com/ioStream/file/file4.txt");

        File[] files = {f2};

        FileService.writeFile(f1, files);
        FileService.toStart(f1, files);
        FileService.toEnd(f1, files);

    }
}
