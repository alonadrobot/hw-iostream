package com.ioStream;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class ServiceTest {
    private final byte[] byte1 = new byte[75];
    private final Service cut = new Service(byte1);
    private final ByteArrayOutputStream byteArrayOutputStream = Mockito.mock(ByteArrayOutputStream.class);
    private final FileInputStream fileInputStream = Mockito.mock(FileInputStream.class);

    @Test
    void writeToOSFromISExceptionTest() {
        assertThrows(IllegalArgumentException.class, () -> cut.write(null, fileInputStream));
        assertThrows(IllegalArgumentException.class, () -> cut.write(byteArrayOutputStream, null));
        assertThrows(IllegalArgumentException.class, () -> cut.write(null, null));
    }

    @Test
    void writeToOSFromISTest() throws IOException {
        int length = 14;
        Mockito.when(fileInputStream.read(byte1)).thenReturn(length).thenReturn(0);

        cut.write(byteArrayOutputStream, fileInputStream);

        Mockito.verify(byteArrayOutputStream, Mockito.times(1)).write(byte1, 0, length);
    }


}