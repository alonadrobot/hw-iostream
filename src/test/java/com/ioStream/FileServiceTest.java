package com.ioStream;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.*;

import static org.junit.jupiter.api.Assertions.*;

class FileServiceTest {
   
    private final File file1 = Mockito.mock(File.class);
    private final File file2 = Mockito.mock(File.class);
    private final File file3 = Mockito.mock(File.class);
    private final ByteArrayOutputStream byteArrayOutputStream = Mockito.mock(ByteArrayOutputStream.class);
    private final FileInputStream fileInputStream = Mockito.mock(FileInputStream.class);
    private final FileOutputStream fileOutputStream = Mockito.mock(FileOutputStream.class);
    private final File[] files = {file2, file3};
    private final File[] empty = new File[0];
    private final File[] newFile = new File[255];
    private final Service Service = Mockito.mock(Service.class);
    private final FileService cut = new FileService(Service);
  

    @Test
    void writeFileTest() throws IOException {
        Mockito.when(Service.getByteArrayOutputStream()).thenReturn(byteArrayOutputStream);
        Mockito.when(Service.getFileInputStream(file2)).thenReturn(fileInputStream);
        Mockito.when(Service.getFileInputStream(file3)).thenReturn(fileInputStream);
        Mockito.when(Service.getFileOutputStream(file1)).thenReturn(fileOutputStream);

        cut.writeFile(file1, files);

        Mockito.verify(Service, Mockito.times(files.length)).write(byteArrayOutputStream, fileInputStream);
        Mockito.verify(byteArrayOutputStream, Mockito.times(1)).writeTo(fileOutputStream);
    }
    @Test
    void toEndTest() throws IOException {
        Mockito.when(Service.getByteArrayOutputStream()).thenReturn(byteArrayOutputStream);
        Mockito.when(Service.getFileOutputStream(file1)).thenReturn(fileOutputStream);
        Mockito.when(Service.getFileInputStream(file1)).thenReturn(fileInputStream);
        Mockito.when(Service.getFileInputStream(file2)).thenReturn(fileInputStream);
        Mockito.when(Service.getFileInputStream(file3)).thenReturn(fileInputStream);

        cut.writeFile(file1, files);

        Mockito.verify(Service, Mockito.times(3)).write(byteArrayOutputStream, fileInputStream);
        Mockito.verify(byteArrayOutputStream, Mockito.times(1)).writeTo(fileOutputStream);
    }
    @Test
    void toEndExceptionTest() {
        assertThrows(IllegalArgumentException.class, () -> cut.toEnd(file1, empty));
        assertThrows(IllegalArgumentException.class, () -> cut.toEnd(file1, newFile));
        assertThrows(IllegalArgumentException.class, () -> cut.toEnd(null, empty));
        assertThrows(IllegalArgumentException.class, () -> cut.toEnd(file1, null));
    }

    @Test
    void writeFileExceptionTest() {
        assertThrows(IllegalArgumentException.class, () -> cut.writeFile(file1, empty));
        assertThrows(IllegalArgumentException.class, () -> cut.toEnd(file1, newFile));
        assertThrows(IllegalArgumentException.class, () -> cut.writeFile(null, empty));
        assertThrows(IllegalArgumentException.class, () -> cut.writeFile(file1, null));
    }

    @Test
    void toStartTest() throws IOException {
        Mockito.when(Service.getByteArrayOutputStream()).thenReturn(byteArrayOutputStream);
        Mockito.when(Service.getFileOutputStream(file1)).thenReturn(fileOutputStream);
        Mockito.when(Service.getFileInputStream(file1)).thenReturn(fileInputStream);
        Mockito.when(Service.getFileInputStream(file2)).thenReturn(fileInputStream);
        Mockito.when(Service.getFileInputStream(file3)).thenReturn(fileInputStream);

        cut.writeFile(file1, files);

        Mockito.verify(Service, Mockito.times(3)).write(byteArrayOutputStream,fileInputStream);
        Mockito.verify(byteArrayOutputStream, Mockito.times(1)).writeTo(fileOutputStream);
    }

    @Test
    void toStartExceptionTest() {
        assertThrows(IllegalArgumentException.class, () -> cut.toStart(file1, empty));
        assertThrows(IllegalArgumentException.class, () -> cut.toEnd(file1, newFile));
        assertThrows(IllegalArgumentException.class, () -> cut.toStart(null, empty));
        assertThrows(IllegalArgumentException.class, () -> cut.toStart(file1, null));
    }




}